<?php
/**
 * Filename: settings.php
 * Created by Barbara Goss
 * Date: 2016-11-27
 * Copyright 2016 Synthose
 */

return [
    'settings' => [
        'displayErrorDetails' => true,
        'addContentLengthHeader' => false,

        'db' => [
            'driver' => 'mysql',
            'host' => 'localhost',
            'database' => 'squirrel',
            'username' => 'root',
            'password' => 'Naw!6acyjna',
            'charset' => 'utf8mb4',
            'collation' => 'utf8mb4_unicode_ci',
            'prefix' => ''
        ]
    ]
];
#!/usr/bin/env bash

# Sets up a Squirrel application environment
#
# Author: Dave Smith-Hayes & Barbara Goss

##
# Variables
##
APP_HOME="/vagrant"
WEBROOT="/var/www"
PHP_CONFIG="/etc/php5"
APACHE_CONFIG="/etc/apache2"
MYSQL_CONFIG="/etc/mysql"

# Provision the software of the server

add-apt-repository -y ppa:ondrej/php5-5.6
apt-get update && apt-get upgrade -y
apt-get install -y apache2 \
                   php5 \
                   php5-dev \
                   libapache2-mod-php5 \
                   php5-json \
                   php5-mysql \
                   php5-sqlite \
                   php5-xdebug \
                   php5-apcu \
                   php-apc
# avoid the MySQL prompts
DEBIAN_FRONTEND=noninteractive apt-get install -y mysql-server mysql-client

if ! grep -q "ServerName Liquidator" $APACHE_CONFIG/apache2.conf; then
	echo "ServerName Liquidator" >> $APACHE_CONFIG/apache2.conf
fi
if ! grep -q "Include /etc/phpmyadmin/apache.conf" $APACHE_CONFIG/apache2.conf; then
	echo "Include /etc/phpmyadmin/apache.conf" >> $APACHE_CONFIG/apache2.conf
fi

if ! [ -d /vagrant/logs ]; then
    mkdir -p $APP_HOME/logs/{apache2,php5.6}
fi

# Fix the php.ini files for the main, and xdebug
if [ -f $PHP_CONFIG/apache2/php.ini ]; then
	rm $PHP_CONFIG/apache2/php.ini
fi
cp $APP_HOME/provisions/configs/php.ini $PHP_CONFIG/apache2/php.ini

if [ -f $PHP_CONFIG/mods-available/xdebug.ini ]; then
	rm $PHP_CONFIG/mods-available/xdebug.ini
fi
cp $APP_HOME/provisions/configs/xdebug.ini $PHP_CONFIG/mods-available/xdebug.ini

# Apache 2.4 directory set up
if ! [ -L $WEBROOT ]; then
    rm -rf $WEBROOT
    ln -fs $APP_HOME/public $WEBROOT    # Slim 3.x webroot
fi

# add mysql encoding
cat $APP_HOME/provisions/configs/mysql_encoding.cnf >> $MYSQL_CONFIG/my.cnf

if ! [ -L $APP_HOME/tmp ]; then
    mkdir $APP_HOME/tmp
    chmod -R 755 $APP_HOME/tmp
fi

# Apache2.4 configuration
cp $APP_HOME/provisions/configs/apache2.conf $APACHE_CONFIG/sites-available/squirrel.conf

a2dissite 000-default
a2ensite squirrel
a2enmod rewrite
service apache2 restart

# Load DB
mysql -u root -p'Naw!6acyjna' < $APP_HOME/provisions/configs/init.sql
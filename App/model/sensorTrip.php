<?php
/**
 * Filename: sensorTrip.php
 * Created by Barbara Goss
 * Date: 2016-11-27
 * Copyright 2016 Synthose
 */

namespace Squirrel\model;

use Illuminate\Database\Eloquent\Model as Model;

class sensorTrip extends Model
{
    protected $table = 'sensor_trips';

/*    protected $attributes = [
        'start_time',
        'end_time'
    ];
*/
}
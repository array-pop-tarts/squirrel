#include <SoftwareSerial.h> 
#include <SparkFunESP8266WiFi.h>
#include <TimeLib.h>

#define PIRPIN 3

const char mySSID[] = "Bandroid";
const char myPSK[] = "squirrel";
const char destServer[] = "aggosst.com";

ESP8266Server server = ESP8266Server(80);

void setup() 
{
  // Serial Monitor is used to control the demo and view
  // debug information.
  Serial.begin(9600);

  pinMode(PIRPIN, INPUT);
  digitalWrite(PIRPIN, LOW);

  //give the sensor some time to calibrate
  Serial.print("calibrating sensor ");
  int calibrationTime = 30;
  for(int i = 0; i < calibrationTime; i++) {
    Serial.print(".");
    delay(1000);
  }
  Serial.println(" done");
  Serial.println("SENSOR ACTIVE");
  delay(50);
   
  initializeESP8266();
  connectESP8266();
  displayConnectInfo();

  setCurrentTime();

  clientDemo();

}

void loop() 
{
  
}

void initializeESP8266()
{
  int test = esp8266.begin();
  if (test != true)
  {
    Serial.println(F("Error talking to ESP8266."));
    errorLoop(test);
  }
  Serial.println(F("ESP8266 Shield Present"));
}

void connectESP8266()
{
  // The ESP8266 can be set to one of three modes:
  //  1 - ESP8266_MODE_STA - Station only
  //  2 - ESP8266_MODE_AP - Access point only
  //  3 - ESP8266_MODE_STAAP - Station/AP combo
  // Use esp8266.getMode() to check which mode it's in:
  int retVal = esp8266.getMode();
  if (retVal != ESP8266_MODE_STA) {
    retVal = esp8266.setMode(ESP8266_MODE_STA);
    if (retVal < 0)
    {
      Serial.println(F("Error setting mode."));
      errorLoop(retVal);
    }
  }
  Serial.println(F("Mode set to station"));

  retVal = esp8266.status();
  if (retVal <= 0)
  {
    Serial.print(F("Connecting to "));
    Serial.println(mySSID);

    retVal = esp8266.connect(mySSID, myPSK);
    if (retVal < 0)
    {
      Serial.println(F("Error connecting"));
      errorLoop(retVal);
    }
  }
  else {
    Serial.println(F("Already connected to "));
    Serial.println(mySSID);
  }
}

void displayConnectInfo()
{
  char connectedSSID[24];
  memset(connectedSSID, 0, 24);
  int retVal = esp8266.getAP(connectedSSID);
  if (retVal > 0)
  {
    Serial.print(F("Connected to: "));
    Serial.println(connectedSSID);
  }
  IPAddress myIP = esp8266.localIP();
  Serial.print(F("My IP: ")); Serial.println(myIP);
}

void setCurrentTime() {
  ESP8266Client client;
  
  int retVal = client.connect(destServer, 80);
  if (retVal <= 0)
  {
    String clientConnectionMessage = "Failed to connect to server. ";
    String clientConnectionError = clientConnectionMessage + retVal;
    Serial.println(clientConnectionError);
    return;
  }
  else {
    Serial.println(F("connected retval: "));
    Serial.println(retVal);
  }
  
  String httpRequest = "GET /current-time HTTP/1.1\n"
                       "Host: aggosst.com\n"
                       "Connection: close\n\n";
  client.print(httpRequest);
  
  char currentTime[11];
  int i = 0;
  
  while (client.available()) {
    currentTime[i] = client.read();
    i++;
  }
    
  setTime(currentTime);
}

void clientDemo()
{
  // To use the ESP8266 as a TCP client, use the 
  // ESP8266Client class. First, create an object:
  ESP8266Client client;

  // ESP8266Client connect([server], [port]) is used to 
  // connect to a server (const char * or IPAddress) on
  // a specified port.
  // Returns: 1 on success, 2 on already connected,
  // negative on fail (-1=TIMEOUT, -3=FAIL).
  int retVal = client.connect(destServer, 80);
  if (retVal <= 0)
  {
    String clientConnectionMessage = "Failed to connect to server. ";
    String clientConnectionError = clientConnectionMessage + retVal;
    Serial.println(clientConnectionError);
    //return;
  }
  else {
    Serial.println(F("connected retval: "));
    Serial.println(retVal);
  }
                             
  int startLoggingThreshold = 5;
  int logTimeoutThreshold = 30;
  
  time_t startTime, endTime;
  char *rawTripTimes[] = {"", ""};
  String postData = "";

  //sensor == HIGH
  startTime = now();
  rawTripTimes[0] = startTime;

  //intercept the startLoggingThreshold
  if (now() - startTime >= startLoggingThreshold) {
    postData = buildTripData(rawTripTimes);
    postSensorTrip(client, postData);
  }

  // sensor == LOW || now() - startTime >= logTimeoutThreshold
  endTime = now();
  rawTripTimes[1] = endTime;

  postData = buildTripData(rawTripTimes);
  postSensorTrip(client, postData);

  // connected() is a boolean return value - 1 if the 
  // connection is active, 0 if it's closed.
  if (client.connected())
    client.stop(); // stop() closes a TCP connection.
}

// errorLoop prints an error code, then loops forever.
void errorLoop(int error)
{
  Serial.print(F("Error: ")); Serial.println(error);
  Serial.println(F("Looping forever."));
  for (;;)
    ;
}

String doubleDigit(int timePart) {
  String fullTimePart = "";
  if (timePart < 10) {
    fullTimePart += "0";
    fullTimePart += timePart;
  }
  else
    fullTimePart = timePart;

  return fullTimePart;
}

String encodedTime(int epochTime) {
  String encodedTime = "";
  encodedTime += year(epochTime);
  encodedTime += "-";
  encodedTime += doubleDigit(month(epochTime));
  encodedTime += "-";
  encodedTime += doubleDigit(day(epochTime));
  encodedTime += "+";
  encodedTime += doubleDigit(hour(epochTime));
  encodedTime += "%3A";
  encodedTime += doubleDigit(minute(epochTime));
  encodedTime += "%3A";
  encodedTime += doubleDigit(second(epochTime));
  return encodedTime;
}

String buildTripData(char *trips[]) {
  String startTime = trips[0];
  String endTime = trips[1];
  
  String tripData = "start_time=";
  if (startTime.length() > 0)
    tripData += encodedTime(startTime.toInt());
  tripData += "&end_time=";
  if (endTime.length() > 0)
    tripData += encodedTime(endTime.toInt());

  return tripData;
}

void postSensorTrip(ESP8266Client client, String postData) {
  String headerDestination = "POST /add-sensor-trip HTTP/1.1\n"
                             "Host: ";
  String headerContentMeta = "Content-Type: application/x-www-form-urlencoded\n"
                             "Content-Length: ";
  client.print(headerDestination);
  client.println(destServer);
  client.print(headerContentMeta);
  client.println(postData.length());
  client.println();
  client.print(postData);
}


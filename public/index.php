<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

require  __DIR__ . '/../vendor/autoload.php';

$settings = require __DIR__ . '/../app/config/settings.php';
$app = new \Slim\App($settings);

require  __DIR__ . '/../app/config/dependencies.php';

$app->getContainer()->get('db');

$app->get('/', function (Request $request, Response $response) {
    $sensorTrips = \Squirrel\model\sensorTrip::all();
    return $this->view->render($response, 'home.html.twig', [
        'time' => '2016-11-24 00:17',
        'sensorTrips' => $sensorTrips
    ]);
})->setName('home');

$app->post('/add-sensor-trip', function (Request $request, Response $response) {

    $params = $request->getParsedBody();

    if (!empty($params['start_time']) || !empty($params['end_time'])) {
        // create new
        if (!empty($params['start_time'])) {
            $sensorTrip = new Squirrel\model\sensorTrip();
            $sensorTrip->start_time = $params['start_time'];

            if (!empty($params['end_time']))
                $sensorTrip->end_time = $params['end_time'];

            $sensorTrip->save();
        } // update existing
        elseif (!empty($params['end_time'])) {
            $sensorTrip = \Squirrel\model\sensorTrip::where('end_time', null)->first();
            $sensorTrip->end_time = $params['end_time'];
            $sensorTrip->save();
        }
    }

    return $response->withStatus(301)->withHeader('Location', $this->router->pathFor('home'));
})->setName('addSensorTrip');

$app->get('/current-time', function(Request $request, Response $response) {
    $currentTime = time();
    $newResponse = $response->withJson($currentTime);
    return $newResponse;
})->setName('getCurrentTime');

$app->run();